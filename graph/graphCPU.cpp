#include <vector>
#include <string>
#include <iostream>
#include <memory>
#include "graph.h"

#include <algorithm>
#include <parser/parser.h>

using namespace std;

/**
 * Generate an Erdos random graph
 * @param n number of nodes
 * @param density probability of an edge (expected density)
 * @param eng seed
 */
template<typename nodeW, typename edgeW>
void Graph<nodeW, edgeW>::setup(node nn) {
	if (GPUEnabled)
		setMemGPU(nn, GPUINIT_NODES);
	else {
		str = new GraphStruct<nodeW, edgeW>;
		str->cumulDegs = new node_sz[nn + 1]{};  // starts by zero
	}
	str->nNodes = nn;
}


/**
 * Generate a new random graph
 * @param eng seed
 */
template<typename nodeW, typename edgeW>
void Graph<nodeW, edgeW>::randGraphUnweighted(float prob,
		std::default_random_engine & eng) {
	if (prob < 0 || prob > 1) {
		printf("[Graph] Warning: Probability not valid (set p = 0.5)!!\n");
//		throw GraphStruct<nodeW,edgeW>::Invalid{};
	}
	uniform_real_distribution<> randR(0.0, 1.0);
	node n = str->nNodes;

	// gen edges
	vector<int>* edges = new vector<int>[n];
	for (uint32_t i = 0; i < n - 1; i++) {
		for (uint32_t j = i + 1; j < n; j++)
			if (randR(eng) < prob) {
				edges[i].push_back(j);
				edges[j].push_back(i);
				str->cumulDegs[i + 1]++;
				str->cumulDegs[j + 1]++;
				str->nEdges += 2;
			}
	}
	str->cumulDegs[0] = 0;
	for (uint32_t i = 0; i < n; i++)
		str->cumulDegs[i + 1] += str->cumulDegs[i];

	// max, min, mean deg
	maxDeg = 0;
	minDeg = n;
	for (uint32_t i = 0; i < n; i++) {
		if (str->deg(i) > maxDeg)
			maxDeg = (uint32_t) str->deg(i);
		if (str->deg(i) < minDeg)
			minDeg = (uint32_t) str->deg(i);
	}
	density = (float) str->nEdges / 2 / (float) (n * (n - 1)/2);
	meanDeg = (float) str->nEdges / 2 / (float) n;
	if (minDeg == 0)
		connected = false;
	else
		connected = true;

	// manage memory for edges with CUDA Unified Memory
	if (GPUEnabled)
		setMemGPU(n, GPUINIT_EDGES);
	else
		str->neighs = new node[str->nEdges] { };

	for (uint32_t i = 0; i < n; i++)
		memcpy((str->neighs + str->cumulDegs[i]), edges[i].data(),
				sizeof(int) * edges[i].size());

	// free resources
	delete[] edges;
}


/**
 * Generate a new graph based on parser
 */
template<typename nodeW, typename edgeW>
void Graph<nodeW, edgeW>::parsedGraph(Parser parser) {

    node n = str->nNodes;

    for(uint32_t i = 0; i < n + 1; i++){
        str->cumulDegs[i] = parser.getCumulDeg(i);
    }

    str->nEdges = parser.getNEdges() * 2;
    // max, min, mean deg
    maxDeg = 0;
    minDeg = n;
    for (uint32_t i = 0; i < n; i++) {
        if (str->deg(i) > maxDeg) {
			maxDeg = (uint32_t) str->deg(i);
		}
        if (str->deg(i) < minDeg)
            minDeg = (uint32_t) str->deg(i);
    }
    density = ((float) str->nEdges) / ((float)n * (n - 1));
    meanDeg = ((float) str->nEdges) / (float) n;
    if (minDeg == 0)
        connected = false;
    else
        connected = true;

    // manage memory for edges with CUDA Unified Memory
    if (GPUEnabled)
        setMemGPU(n, GPUINIT_EDGES);
    else
        str->neighs = new node[str->nEdges] { };

    for (uint32_t i = 0; i < str->nEdges; i++)
       str->neighs[i] = parser.getNeigh(i);
}

/**
 * Print the graph (verbose = 1 for "verbose print")
 * @param verbose print the complete graph
 */
template<typename nodeW, typename edgeW>
void Graph<nodeW, edgeW>::print(bool verbose) {
	node n = str->nNodes;
	cout << "\n** Graph " << endl;
	cout << "   - num node: " << n << endl;
	cout << "   - num edges: " << str->nEdges / 2 << " (density: " << density << ")"<< endl;
	cout << "   - min deg: " << minDeg << endl;
	cout << "   - max deg: " << maxDeg << endl;
	cout << "   - mean deg: " << meanDeg << endl;
	cout <<	"   - connected: " << connected << endl;
	if (verbose) {
		for (uint32_t i = 0; i < n; i++) {
			cout << "   node(" << i << ")" << "["
					<< str->cumulDegs[i + 1] - str->cumulDegs[i] << "]-> ";
			for (int j = 0; j < str->cumulDegs[i + 1] - str->cumulDegs[i]; j++) {
				cout << str->neighs[str->cumulDegs[i] + j] << " ";
			}
			cout << "\n";
		}
		cout << "\n";
	}
}
