#pragma once
#include <random>
#ifdef WIN32
#include <cinttypes>
#endif
#include "GPUutils/GPURandomizer.h"
#include <parser/parser.h>

typedef uint32_t node;     // graph node
typedef uint64_t node_sz;

// This should be moved somewhere else
enum GPUMEMINITTYPES {
    GPUINIT_NODES = 1,
    GPUINIT_EDGES = 2,
    GPUINIT_NODEW = 3,
    GPUINIT_EDGEW = 4
};

/**
 * Base structure (array 1D format) of a graph with weighted nodes/edges
 */
template<typename nodeW, typename edgeW> struct GraphStruct {
	node       nNodes{0};             // num of graph nodes
	node_sz    nEdges{0};             // num of graph edges
	node_sz*   cumulDegs{};           // cumsum of node degrees
	node*      neighs{};              // list of neighbors for all nodes (edges)
	nodeW*     nodeWeights{};         // list of weights for all nodes
	edgeW*     edgeWeights{};         // list of weights for all edges

	~GraphStruct() {delete[] neighs; delete[] cumulDegs; delete[] nodeWeights; delete[] edgeWeights;}

	class Invalid {};

	bool is_valid() {
		for (int i = 0; i < nEdges; i++)
			if (neighs[i] > nNodes - 1)   // inconsistent neighbor index
				return false;
		if (cumulDegs[nNodes] != nEdges)  // inconsistent number of edges
			return false;
		return true;
	};

	/// return the degree of node i
	node_sz deg(node i) {
		return( cumulDegs[i + 1] - cumulDegs[ i ] );
	}

	/// check whether node i is a neighbor of node j
	bool areNeighbor(node i, node j) {
		for (uint32_t k = 0; k < deg( j ); k++) {
			if (neighs[cumulDegs[j] + k] == i)
				return true;
		}
		return false;
	}

};

/**
 * It manages a graph for CPU & GPU
 */
template<typename nodeW, typename edgeW> class Graph {
	float density{0.0f};	                       /// Probability of an edge (Erdos graph)
	GraphStruct<nodeW, edgeW>* str{};      /// graph structure
	node maxDeg{0};
	node minDeg{0};
	float meanDeg{0.0f};
	bool connected{true};
	bool GPUEnabled{true};

public:
	Graph(node nn, bool GPUEnb) : GPUEnabled{GPUEnb} {setup(nn);}
	~Graph() {if (GPUEnabled) deleteMemGPU(); else delete str;};
	void setup(node);	 /// CPU/GPU mem setup
	void randGraphUnweighted(float prob, std::default_random_engine&);  /// generate an Erdos random graph
	void parsedGraph(Parser);  /// generate parsed graph
	void print(bool);
	void print_d(bool);
	GraphStruct<nodeW,edgeW>* getStruct() {return str;}
	void setMemGPU(node_sz nn, int mode);                    /// use UVA memory on CPU/GPU
	void deleteMemGPU();
	void enableGPU() {GPUEnabled = true;}
	void disableGPU() {GPUEnabled = false;}
	bool isGPUEnabled() {return GPUEnabled;}
	node getMaxNodeDeg() {return maxDeg;};
	void efficiency();
	void parse();

};
