// HyperSMURF sequenziale
// Alessandro Petrini, 2017
#include <iostream>
#include "ArgHandle.h"
#include <getopt.h>

void ArgHandle::processCommandLine() {

	char const *short_options = "f:a:n:p:l:e:x:s:h";
	const struct option long_options[] = {

		{ "filename",	required_argument, 0, 'f' },
		{ "algonum",	required_argument, 0, 'a' },
		{ "numThreads",	required_argument, 0, 'n' },
		{ "prob",		required_argument, 0, 'p' },
		{ "lambda",		required_argument, 0, 'l' },
		{ "epsilon",	required_argument, 0, 'e' },
		{ "exec",   	required_argument, 0, 'x' },
		{ "seed",		required_argument, 0, 's' },
		{ "help",		required_argument, 0, 'h' },
		{ 0, 0, 0, 0 }
	};

	algs_enbl[0] = false; algs_enbl[1] = false; algs_enbl[2] = true;
	Alg_name[0] = "GreedyCPU"; Alg_name[1] = "LubyGPU"; Alg_name[2] = "MCMCGPU";

	std::string alg;
	// loop on options
	while (1) {
		int option_index = 0;
		int c = getopt_long( argc, argv, short_options, long_options, &option_index );

		if (c == -1)
			break;
		switch (c) {
		case 'n':
			try {
				int temp = std::stoi( optarg );
				if (temp < 1) {
					std::cout << "\033[31;1mThreads per block must be positive (--nodes).\033[0m" << std::endl;
					abort();
				}
				else
					n = temp;
			}
			catch (...) {
				std::cout << "\033[31;1mThreads per block must be positive (--nodes).\033[0m" << std::endl;
				abort();
			}
			break;
		case 'p':
			try {
				float temp = std::stof( optarg );
				if ((temp < 0) | (temp > 1)) {
					std::cout << "\033[31;1mSimulation: probabilty of positive class must be 0 < prob < 1 (--prob).\033[0m" << std::endl;
					abort();
				}
				else
					P = temp;
			}
			catch (...) {
				std::cout << "\033[31;1mArgument missing: specify the probabilty for positive class (--prob).\033[0m" << std::endl;
				abort();
			}
			break;
		case 'l':
			try {
				float temp = std::stof( optarg );
				if (temp < 0) {
					std::cout << "\033[31;1m  Qui manca il commento sulla lambda (--lambda).\033[0m" << std::endl;
					abort();
				}
				else
					lambda = temp;
			}
			catch (...) {
				std::cout << "\033[31;1m  Qui manca il commento sulla lambda (--lambda).\033[0m" << std::endl;
				abort();
			}
			break;
		case 'e':
			try {
				float temp = std::stof( optarg );
				if (temp < 0) {
					std::cout << "\033[31;1m  Qui manca il commento sulla epsilon (--epsilon).\033[0m" << std::endl;
					abort();
				}
				else {
					epsilon = temp;
				}
			}
			catch (...) {
				std::cout << "\033[31;1m  Qui manca il commento sulla epsilon (--epsilon).\033[0m" << std::endl;
				abort();
			}
			break;
		case 's':
			try {
				int temp = std::stoi( optarg );
				if (temp < 1) {
					std::cout << "\033[31;1mseed value must be positive (--seed).\033[0m" << std::endl;
					abort();
				}
				else
					seed = temp;
			}
			catch (...) {
				std::cout << "\033[31;1mseed value must be positive (--nodes).\033[0m" << std::endl;
				abort();
			}
			break;
		case 'x':
			alg = std::string(optarg);
			if (alg.compare(Alg_name[Algs::GreedyCPU]))     // "GreedyCPU"
				algs_enbl[Algs::GreedyCPU] = 1;
			else if (alg.compare(Alg_name[Algs::LubyGPU]))  // "LubyGPU"
				algs_enbl[Algs::LubyGPU] = 1;
			else if (alg.compare(Alg_name[Algs::MCMCGPU]))  // "MCMCGPU"
				algs_enbl[Algs::MCMCGPU] = 1;
			break;
		case 'h':
			displayHelp();
			exit( 0 );
		case 'f':
			try {
				filename = optarg;
			}
			catch (...) {
				std::cout << "Errore lettura filename" << std::endl;
				abort();
			}
			break;
		case 'a':
			algonum = static_cast<uint32_t>(std::stoi(optarg ));
			break;
		default:
			break;
		}
	}

	if (seed == 0)
		seed = (uint32_t) time( NULL );

}

void ArgHandle::displayHelp() {
	std::cout << " **** Graph Coloring ****" << std::endl;
	std::cout << "A CUDA implementation of a parallel coloring algorithm based on the MCMC method" << std::endl;
	std::cout << "Usage: " << std::endl;
	std::cout << "    " << argv[0] << " [options]" << std::endl;
	std::cout << std::endl;

	std::cout << "Options:" << std::endl;
	std::cout << "    " << "--help          Print this help." << std::endl;
	std::cout << "    " << "-f filename     load graph by file." << std::endl;
	std::cout << "    " << "-n N            num threads." << std::endl;
	std::cout << "    " << "-p prob         edge probability [0:1] for random graph." << std::endl;
	std::cout << "    " << "-l lam          lambda param [>0] of prior color distribution" << std::endl;
	std::cout << "    " << "-e eps          epsilon param [>0] for MCMC method" << std::endl;
	std::cout << "    " << "-s seed         for random generation" << std::endl;
	std::cout << "    " << "-x alg          exec algorithm" << std::endl;
	std::cout << "    " << "-v              verbose" << std::endl;
	std::cout << "    " << "-a alg          select gpu algorithm" << std::endl;

	std::cout << std::endl << std::endl;
}
