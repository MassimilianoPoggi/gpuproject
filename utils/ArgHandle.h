#pragma once
#include <string>
#include <cinttypes>
#include <stdexcept>
#include <ctime>

// algorithms included for coloring
enum Algs {GreedyCPU, LubyGPU, MCMCGPU};

// Class for processing command line arguments
class ArgHandle {
public:
	ArgHandle( int argc, char **argv ) : argc( argc ), argv( argv ) {}
	~ArgHandle(){};
	void processCommandLine();

	std::string		filename{};			// graph stored in file
	uint32_t 		algonum{};
	uint32_t		n{64};				// num of nodes
	float			P{0.1f};			// prob of an edge (for random graph)
	float			lambda{1.0f};
	float			epsilon{1e-4f};
	uint32_t		seed{0};
	bool			verbose{0};
	bool			algs_enbl[3];
	std::string		Alg_name[3];

private:
	void displayHelp();
	int				argc;
	char		**	argv;

};
