
#include "parser.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <coloring/coloring.h>
#include "graph/graph.h"
#include "graph/graphCPU.cpp"

/* parser a file containing:
 * nnodes, nnodes, nedges
 * list edges (a b) sorted
 * NB: nodes IDs start from 1
 */
void Parser::parse(std::string filename) {
    std::string line;

    std::ifstream inputFileStream(filename);

    if(!inputFileStream){
        std::cerr << "can't read file "<<filename;
    }
    std::getline(inputFileStream, line);
    nNodes = static_cast<uint32_t>(std::stoi(line.substr(0, line.find(' '))));
    nEdges = static_cast<uint64_t>(std::stoi(line.substr(line.find_last_of(' ') + 1)));

    cumulDegs = (uint64_t *)malloc(sizeof(uint64_t)*(nNodes+1));
    neighs = (uint32_t *)malloc(sizeof(uint32_t) * nEdges * 2);
    uint32_t neighs_pointer = 0;

    while(std::getline(inputFileStream, line)) {
        //reads "a b"
        uint32_t a = static_cast<uint32_t>(std::stoi(line.substr(0, line.find(' '))));
        uint32_t b = static_cast<uint32_t>(std::stoi(line.substr(line.find(' ')+1)));
        //adds "b" to neighs (already sorted in formatted file)
        neighs[neighs_pointer] = b - 1;
        neighs_pointer++;

        cumulDegs[a]++;
    }

    cumulDegs[0] = 0;
    for (uint32_t i = 1; i < nNodes + 1; i++)
        cumulDegs[i] += cumulDegs[i-1];

    inputFileStream.close();
}

