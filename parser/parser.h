#pragma once
#include <random>

#ifdef WIN32
#include <cinttypes>
#endif


class Parser {
    uint32_t       nNodes;             // num of graph nodes
    uint64_t    nEdges;             // num of graph edges
    uint64_t *   cumulDegs{};           // cumsum of node degrees
    uint32_t *      neighs{};             // list of neighbors for all nodes (edges)
public:
    void parse(std::string filename);
    uint32_t getNNodes(){return nNodes;}
    uint64_t getNEdges(){return nEdges;}
    uint64_t getCumulDeg(uint32_t i){return cumulDegs[i];}
    uint32_t getNeigh(uint32_t i){return neighs[i];}

};