==141117== NVPROF is profiling process 141117, command: ./cmake-build-debug/colorerCh -f files/graphs/delaunay_n19.txt -n 256 -a 1
algo: 1
aggiornato7

** Graph 
   - num node: 524288
   - num edges: 1572823 (density: 1.14438e-05)
   - min deg: 3
   - max deg: 21
   - mean deg: 5.99984
   - connected: 1
==141117== Some kernel(s) will be replayed on device 0 in order to collect all events/metrics.

[32;1m**    RUN!   **[0m
LUBY STRUCT INDEX
GPU time: 785.717
Max color: 1062
calling cudaDeviceReset()...
Done.
==141117== Profiling application: ./cmake-build-debug/colorerCh -f files/graphs/delaunay_n19.txt -n 256 -a 1
==141117== Profiling result:
==141117== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Tesla K40c (0)"
    Kernel: lubyIndexesColor(unsigned int*, unsigned int, unsigned int const *, unsigned int, unsigned int)
       1062                        achieved_occupancy                        Achieved Occupancy    0.065670    0.517545    0.095172
       1062                            gld_efficiency             Global Memory Load Efficiency      12.50%     100.00%      63.41%
       1062                            gst_efficiency            Global Memory Store Efficiency      12.50%      25.00%      14.17%
       1062                 warp_execution_efficiency                 Warp Execution Efficiency      86.54%     100.00%      93.04%
    Kernel: lubyIndexesToDoPermutation(GraphStruct<unsigned int, unsigned int> const *, unsigned int, unsigned int const *, unsigned int const *, unsigned int*)
       1062                        achieved_occupancy                        Achieved Occupancy    0.085236    0.817156    0.304357
       1062                            gld_efficiency             Global Memory Load Efficiency      12.50%      90.28%      74.66%
       1062                            gst_efficiency            Global Memory Store Efficiency      12.50%      99.36%      79.80%
       1062                 warp_execution_efficiency                 Warp Execution Efficiency      88.32%     100.00%      99.40%
    Kernel: void thrust::cuda_cub::core::_kernel_agent<thrust::cuda_cub::__scan::InitAgent<thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>, int>, thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>, int>(bool=1, thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>)
       1062                        achieved_occupancy                        Achieved Occupancy    0.053526    0.060647    0.057029
       1062                            gld_efficiency             Global Memory Load Efficiency       0.00%       0.00%       0.00%
       1062                            gst_efficiency            Global Memory Store Efficiency      45.83%      50.00%      47.65%
       1062                 warp_execution_efficiency                 Warp Execution Efficiency      88.60%     100.00%      90.86%
    Kernel: void thrust::cuda_cub::core::_kernel_agent<thrust::cuda_cub::__scan::ScanAgent<unsigned int*, unsigned int*, thrust::plus<unsigned int>, int, unsigned int, thrust::detail::integral_constant<bool, bool=1>>, unsigned int*, unsigned int*, thrust::plus<unsigned int>, int, thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>, thrust::cuda_cub::__scan::DoNothing<unsigned int>>(unsigned int*, unsigned int, thrust::plus<unsigned int>, int, unsigned int, bool)
       1062                        achieved_occupancy                        Achieved Occupancy    0.058384    0.552749    0.099150
       1062                            gld_efficiency             Global Memory Load Efficiency       0.00%      91.43%      59.93%
       1062                            gst_efficiency            Global Memory Store Efficiency      12.50%     100.00%      81.71%
       1062                 warp_execution_efficiency                 Warp Execution Efficiency      98.09%      99.54%      98.65%
    Kernel: lubyIndexesIsToColor(GraphStruct<unsigned int, unsigned int> const *, unsigned int const *, unsigned int*, unsigned int, unsigned int const *)
       1062                        achieved_occupancy                        Achieved Occupancy    0.026653    0.874890    0.280487
       1062                            gld_efficiency             Global Memory Load Efficiency      14.50%      27.42%      24.90%
       1062                            gst_efficiency            Global Memory Store Efficiency      12.50%     100.00%      82.05%
       1062                 warp_execution_efficiency                 Warp Execution Efficiency      41.10%      69.93%      59.33%
    Kernel: lubyIndexesToDoIndirectPermutation(GraphStruct<unsigned int, unsigned int> const *, unsigned int, unsigned int*, unsigned int*, unsigned int)
       1062                        achieved_occupancy                        Achieved Occupancy    0.077086    0.845236    0.305730
       1062                            gld_efficiency             Global Memory Load Efficiency      12.50%      89.97%      73.24%
       1062                            gst_efficiency            Global Memory Store Efficiency      12.50%     100.00%      82.05%
       1062                 warp_execution_efficiency                 Warp Execution Efficiency      83.85%     100.00%      99.27%
    Kernel: lubyIndexesInitToDo(GraphStruct<unsigned int, unsigned int> const *, unsigned int*)
          1                        achieved_occupancy                        Achieved Occupancy    0.841426    0.841426    0.841426
          1                            gld_efficiency             Global Memory Load Efficiency      12.50%      12.50%      12.50%
          1                            gst_efficiency            Global Memory Store Efficiency     100.00%     100.00%     100.00%
          1                 warp_execution_efficiency                 Warp Execution Efficiency     100.00%     100.00%     100.00%
    Kernel: GPURand_k::initCurand(curandStateXORWOW*, unsigned int, unsigned int)
          1                        achieved_occupancy                        Achieved Occupancy    0.249020    0.249020    0.249020
          1                            gld_efficiency             Global Memory Load Efficiency      21.38%      21.38%      21.38%
          1                            gst_efficiency            Global Memory Store Efficiency      23.21%      23.21%      23.21%
          1                 warp_execution_efficiency                 Warp Execution Efficiency      99.16%      99.16%      99.16%
