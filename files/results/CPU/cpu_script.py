import sys

algo_map = {'0': 'Luby simple index', '1': 'Luby struct index', 
		'2': 'Luby simple random', '3': 'Luby struct random',
		'4': 'Plassman simple', '5': 'Plassman struct', 
		'6': 'Largest simple', '7': 'Largest struct'}
thread_l = ['32', '64', '128', '256', '512', '1024']
graph_l = ['rand10k', 'rand25k', 'rand50k', 'rand100k', 'delaunay_n19']

for g in graph_l:
	res = list()
	with open('{}.results'.format(g), 'r') as r:
		lines = list(filter(lambda l: 'CPU time' in l, r.readlines()))
		lines = [float(x[10:-1]) for x in lines]
		print(g, sum(lines)/len(lines))

