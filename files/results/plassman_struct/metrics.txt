==157855== NVPROF is profiling process 157855, command: ./build/cmake-debug/colorerCh -f files/graphs/delaunay_n19.txt -n 256 -a 5

** Graph 
   - num node: 524288
   - num edges: 1572823 (density: 1.14438e-05)
   - min deg: 3
   - max deg: 21
   - mean deg: 5.99984
   - connected: 1
==157855== Some kernel(s) will be replayed on device 0 in order to collect all events/metrics.

[32;1m**    RUN!   **[0m
GPU time: 12.9703
Max color: 19
calling cudaDeviceReset()...
Done.
==157855== Profiling application: ./build/cmake-debug/colorerCh -f files/graphs/delaunay_n19.txt -n 256 -a 5
==157855== Profiling result:
==157855== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Tesla K40c (0)"
    Kernel: jonesPlassmanToDoPermutation(GraphStruct<unsigned int, unsigned int> const *, unsigned int, unsigned int const *, unsigned int const *, unsigned int*)
         19                        achieved_occupancy                        Achieved Occupancy    0.094082    0.814825    0.475051
         19                            gld_efficiency             Global Memory Load Efficiency      12.50%      90.28%      65.19%
         19                            gst_efficiency            Global Memory Store Efficiency      12.50%      81.25%      64.85%
         19                 warp_execution_efficiency                 Warp Execution Efficiency      88.26%     100.00%      97.86%
    Kernel: void thrust::cuda_cub::core::_kernel_agent<thrust::cuda_cub::__scan::InitAgent<thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>, int>, thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>, int>(bool=1, thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>)
         19                        achieved_occupancy                        Achieved Occupancy    0.055897    0.060570    0.057944
         19                            gld_efficiency             Global Memory Load Efficiency       0.00%       0.00%       0.00%
         19                            gst_efficiency            Global Memory Store Efficiency      45.83%      50.00%      48.25%
         19                 warp_execution_efficiency                 Warp Execution Efficiency      88.60%     100.00%      93.53%
    Kernel: jonesPlassmanToDoIndirectPermutation(GraphStruct<unsigned int, unsigned int> const *, unsigned int, unsigned int*, unsigned int*, unsigned int)
         19                        achieved_occupancy                        Achieved Occupancy    0.072730    0.845224    0.479828
         19                            gld_efficiency             Global Memory Load Efficiency      12.50%      67.07%      48.37%
         19                            gst_efficiency            Global Memory Store Efficiency      12.50%     100.00%      75.33%
         19                 warp_execution_efficiency                 Warp Execution Efficiency      83.85%     100.00%      97.11%
    Kernel: void thrust::cuda_cub::core::_kernel_agent<thrust::cuda_cub::__scan::ScanAgent<unsigned int*, unsigned int*, thrust::plus<unsigned int>, int, unsigned int, thrust::detail::integral_constant<bool, bool=1>>, unsigned int*, unsigned int*, thrust::plus<unsigned int>, int, thrust::cuda_cub::cub::ScanTileState<unsigned int, bool=1>, thrust::cuda_cub::__scan::DoNothing<unsigned int>>(unsigned int*, unsigned int, thrust::plus<unsigned int>, int, unsigned int, bool)
         19                        achieved_occupancy                        Achieved Occupancy    0.059264    0.547125    0.221950
         19                            gld_efficiency             Global Memory Load Efficiency       0.00%      91.43%      54.38%
         19                            gst_efficiency            Global Memory Store Efficiency      12.50%      99.23%      75.00%
         19                 warp_execution_efficiency                 Warp Execution Efficiency      98.10%      99.54%      98.61%
    Kernel: jonesPlassmanIsToColor(GraphStruct<unsigned int, unsigned int> const *, unsigned int const *, unsigned int const *, unsigned int*, unsigned int, unsigned int const *)
         19                        achieved_occupancy                        Achieved Occupancy    0.026785    0.918560    0.508729
         19                            gld_efficiency             Global Memory Load Efficiency      14.42%      29.01%      18.57%
         19                            gst_efficiency            Global Memory Store Efficiency      12.50%     100.00%      75.33%
         19                 warp_execution_efficiency                 Warp Execution Efficiency      39.75%      65.85%      54.48%
    Kernel: jonesPlassmanGenerateRandom(GraphStruct<unsigned int, unsigned int> const *, unsigned int*, curandStateXORWOW*)
          1                        achieved_occupancy                        Achieved Occupancy    0.799463    0.799463    0.799463
          1                            gld_efficiency             Global Memory Load Efficiency      24.87%      24.87%      24.87%
          1                            gst_efficiency            Global Memory Store Efficiency      28.00%      28.00%      28.00%
          1                 warp_execution_efficiency                 Warp Execution Efficiency     100.00%     100.00%     100.00%
    Kernel: GPURand_k::initCurand(curandStateXORWOW*, unsigned int, unsigned int)
          1                        achieved_occupancy                        Achieved Occupancy    0.249282    0.249282    0.249282
          1                            gld_efficiency             Global Memory Load Efficiency      21.37%      21.37%      21.37%
          1                            gst_efficiency            Global Memory Store Efficiency      23.21%      23.21%      23.21%
          1                 warp_execution_efficiency                 Warp Execution Efficiency      99.16%      99.16%      99.16%
    Kernel: jonesPlassmanInitToDo(GraphStruct<unsigned int, unsigned int> const *, unsigned int*)
          1                        achieved_occupancy                        Achieved Occupancy    0.844708    0.844708    0.844708
          1                            gld_efficiency             Global Memory Load Efficiency      12.50%      12.50%      12.50%
          1                            gst_efficiency            Global Memory Store Efficiency     100.00%     100.00%     100.00%
          1                 warp_execution_efficiency                 Warp Execution Efficiency     100.00%     100.00%     100.00%
    Kernel: jonesPlassmanColor(unsigned int*, unsigned int, unsigned int const *, unsigned int, unsigned int)
         19                        achieved_occupancy                        Achieved Occupancy    0.073997    0.694827    0.367420
         19                            gld_efficiency             Global Memory Load Efficiency      12.50%      99.99%      73.33%
         19                            gst_efficiency            Global Memory Store Efficiency      12.50%      18.69%      14.81%
         19                 warp_execution_efficiency                 Warp Execution Efficiency      86.54%     100.00%      97.49%
