#include <iostream>
#ifdef WIN32
#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>
#include <device_launch_parameters.h>
#endif
#include "graph/graph.h"
#include "graph/graphCPU.cpp"
#include "graph/graphGPU.cu"
#include "coloring/coloring.h"
#include "coloring/ColorerCh.h"
#include "utils/ArgHandle.h"
#include "GPUutils/GPUutils.h"
#include "GPUutils/GPUCudaCleaner.h"
#include "parser/parser.h"
#include <fstream>

// Initializing temporary static stuff for collecting stats. Should be implemented as a proper class.
// As now, haters gonna hate.
uint64_t GPUMemTracker::graphStructSize			= 0;
uint64_t GPUMemTracker::graphDegsSize			= 0;
uint64_t GPUMemTracker::graphNeighsSize			= 0;
uint64_t GPUMemTracker::graphNodeWSize			= 0;
uint64_t GPUMemTracker::graphEdgeWSize			= 0;
uint64_t GPUMemTracker::coloringColorsSize		= 0;


int main( int argc, char *argv[] ) {

	CudaCleaner CudaClean;		// Invoca cudaDeviceReset() automaticamente al termine del programma

	ArgHandle args( argc, argv );
	args.processCommandLine();

	uint32_t seed = args.seed;	// se non specificato, seed casuale
	string file = args.filename;
	uint32_t algonum = args.algonum;
	uint32_t threadsPerBlock = args.n;

	bool GPUEnabled = 1;

    Parser p;
    p.parse(file);
    Graph<col,col> graph {p.getNNodes(), GPUEnabled};
    graph.parsedGraph(p);
	graph.print( false );

	GPURand curandGen( graph.getStruct()->nNodes, (unsigned long) seed );

	ColorerCh colorerCh {&graph, &curandGen};

	colorerCh.run(threadsPerBlock, algonum);
	colorerCh.testCorrectColoring(false);

	//Whatever...
	return EXIT_SUCCESS;
}
