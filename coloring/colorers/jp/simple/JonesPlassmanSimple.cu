#include "ColorerCh.h"
#include "ColorerCh.h"

__global__ void jonesPlassmanIsToColor(const GraphStruct<col, col> *str, const uint32_t *C, const uint32_t *R,
                                    bool* toColor_d) {
    uint32_t nodeId = threadIdx.x + blockDim.x * blockIdx.x;

    if (nodeId < str->nNodes && C[nodeId] == 0) {
        bool toColor = true;
        for (node_sz i = str->cumulDegs[nodeId]; (i < str->cumulDegs[nodeId + 1]) && toColor; i++) {
            uint32_t neighbourId = str->neighs[i];
            toColor = (R[neighbourId] < R[nodeId] ||
                      (R[neighbourId] == R[nodeId] && neighbourId < nodeId) ||
                      C[neighbourId] != 0);
        }

        toColor_d[nodeId] = toColor;
    }
}

__global__ void jonesPlassmanExtraction(const GraphStruct<col, col> *str, uint32_t * const R, curandState *GPUrnd) {
    uint32_t nodeId = threadIdx.x + blockDim.x * blockIdx.x;

    if (nodeId < str->nNodes) {
        R[nodeId] = curand(&GPUrnd[nodeId]);
    }
}

__global__ void jonesPlassmanColor(const GraphStruct<col, col> *str, uint32_t * const C, bool *toColor, uint32_t currentColor, uint32_t* missingColor) {
    uint32_t nodeId = threadIdx.x + blockDim.x * blockIdx.x;

    if (nodeId < str->nNodes && C[nodeId] == 0) {
        if (toColor[nodeId])
            C[nodeId] = currentColor;
        else
            (*missingColor)++;
    }
}

void ColorerChKernels::jonesPlassmanSimple(const GraphStruct<col, col> * const str, uint32_t * const C, curandState * GPUrnd, uint32_t threadsPerBlock_x) {
    //save because of UM synchronize requirement
    uint32_t nNodes = str->nNodes;

    uint32_t *R_d;
    cudaError cuSts = cudaMalloc(&R_d, sizeof(uint32_t) * nNodes);
    cudaCheck(cuSts, __FILE__, __LINE__);

    bool *toColor_d;
    cuSts = cudaMalloc(&toColor_d, sizeof(bool) * nNodes);
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t *missingColor_d;
    cuSts = cudaMalloc(&missingColor_d, sizeof(uint32_t));
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t currentColor = 0;

    uint32_t missingColor = 0;
    uint32_t missingColor_old = 0;

    dim3 threadsPerBlock = dim3(threadsPerBlock_x, 1, 1);
    dim3 blocksPerGrid = dim3((nNodes + threadsPerBlock.x - 1) / threadsPerBlock.x, 1, 1);

    jonesPlassmanExtraction <<< blocksPerGrid, threadsPerBlock >>> (str, R_d, GPUrnd);

    do {
        currentColor++;

        jonesPlassmanIsToColor <<< blocksPerGrid, threadsPerBlock >>> (str, C, R_d, toColor_d);

        jonesPlassmanColor <<< blocksPerGrid, threadsPerBlock >>> (str, C, toColor_d, currentColor, missingColor_d);

        missingColor_old = missingColor;
        cuSts = cudaMemcpy(&missingColor, missingColor_d, sizeof(uint32_t), cudaMemcpyDeviceToHost);
        cudaCheck(cuSts, __FILE__, __LINE__);

    } while(missingColor_old != missingColor);

    cudaFree(missingColor_d);
    cudaFree(toColor_d);
    cudaFree(R_d);
}
