#include "ColorerCh.h"
#include "ColorerCh.h"

__global__ void lubyIndexesIsToColor(const GraphStruct<col, col> *str, const uint32_t *C, bool *toColor_d) {
    uint32_t nodeId = threadIdx.x + blockDim.x * blockIdx.x;

    if (nodeId < str->nNodes && C[nodeId] == 0) {
        bool toColor = true;
        for (node_sz i = str->cumulDegs[nodeId]; (i < str->cumulDegs[nodeId + 1]) && toColor; i++) {
            uint32_t neighbourId = str->neighs[i];
            toColor = neighbourId < nodeId || C[neighbourId] != 0;
        }

        toColor_d[nodeId] = toColor;
    }
}

__global__ void lubyIndexesColor(const GraphStruct<col, col> *str, uint32_t *const C, bool *toColor,
                                 uint32_t currentColor, uint32_t *missingColor) {
    uint32_t nodeId = threadIdx.x + blockDim.x * blockIdx.x;
    if (nodeId < str->nNodes && C[nodeId] == 0) {
        if (toColor[nodeId])
            C[nodeId] = currentColor;
        else
            (*missingColor)++;
    }
}

void ColorerChKernels::lubyIndexesSimple(const GraphStruct<col, col> * const str, uint32_t * const C, uint32_t threadsPerBlock_x) {
    uint32_t nNodes = str->nNodes;

    bool *toColor_d;
    cudaError_t cuSts = cudaMalloc(&toColor_d, sizeof(bool) * nNodes);
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t *missingColor_d;
    cuSts = cudaMalloc(&missingColor_d, sizeof(uint32_t));
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t currentColor = 0;

    uint32_t missingColor = 0;
    uint32_t missingColor_old = 0;

    dim3 threadsPerBlock = dim3(threadsPerBlock_x, 1, 1);
    dim3 blocksPerGrid = dim3((nNodes + threadsPerBlock.x - 1) / threadsPerBlock.x, 1, 1);

    do {
        currentColor++;

        lubyIndexesIsToColor <<< blocksPerGrid, threadsPerBlock >>> (str, C, toColor_d);

        lubyIndexesColor <<< blocksPerGrid, threadsPerBlock >>> (str, C, toColor_d, currentColor, missingColor_d);

        missingColor_old = missingColor;
        cuSts = cudaMemcpy(&missingColor, missingColor_d, sizeof(uint32_t), cudaMemcpyDeviceToHost);
        cudaCheck(cuSts, __FILE__, __LINE__);
    } while(missingColor_old != missingColor);

    cudaFree(missingColor_d);
    cudaFree(toColor_d);
}
