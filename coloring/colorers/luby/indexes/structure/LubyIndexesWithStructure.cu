#include "ColorerCh.h"

#include <thrust/scan.h>
#include <thrust/execution_policy.h>

__global__ void lubyIndexesInitToDo(const GraphStruct<col, col> *str, uint32_t *const toDo) {
    uint32_t nodeId = threadIdx.x + blockDim.x * blockIdx.x;

    if (nodeId < str->nNodes) {
        toDo[nodeId] = nodeId;
    }
}

__global__ void lubyIndexesIsToColor(const GraphStruct<col, col> *str, const uint32_t *C,
                                    uint32_t *const toColor, const uint32_t toDoStart, const uint32_t *toDo) {
    uint32_t threadId = threadIdx.x + blockDim.x * blockIdx.x;
    uint32_t toDoOffset = threadId + toDoStart;

    if (toDoOffset < str->nNodes) {
        uint32_t isToColor = 1;
        uint32_t nodeId = toDo[toDoOffset];
        for (node_sz i = str->cumulDegs[nodeId]; (i < str->cumulDegs[nodeId + 1]) && isToColor; i++) {
            uint32_t neighbourId = str->neighs[i];
            isToColor = (neighbourId < nodeId || C[neighbourId] != 0) ? 1 : 0;
        }

        toColor[toDoOffset] = isToColor;
    }
}

__global__ void lubyIndexesToDoIndirectPermutation(const GraphStruct<col, col> *str, const uint32_t toDoStart,
                                                  uint32_t *const shuffle,
                                                  uint32_t *const toColor, uint32_t maxColored) {
    uint32_t threadId = threadIdx.x + blockDim.x * blockIdx.x;
    uint32_t toDoOffset = threadId + toDoStart;

    if (toDoOffset < str->nNodes) {
        uint32_t isToColor = toColor[toDoOffset];
        uint32_t toDoIndex = toDoStart + (isToColor ? (shuffle[toDoOffset] - 1) : (maxColored + threadId - shuffle[toDoOffset]));
        shuffle[toDoOffset] = toDoIndex;
    }
}

__global__ void lubyIndexesToDoPermutation(const GraphStruct<col, col> *str, const uint32_t toDoStart, const uint32_t *shuffle,
                                            const uint32_t *toDo, uint32_t *const toDoPermutated) {
    uint32_t threadId = threadIdx.x + blockDim.x * blockIdx.x;
    uint32_t toDoOffset = threadId + toDoStart;

    if (toDoOffset < str->nNodes) {
        uint32_t toDoIndex = shuffle[toDoOffset];
        toDoPermutated[toDoIndex] = toDo[toDoOffset];
    }
}

__global__ void lubyIndexesColor(uint32_t *const C, const uint32_t currentColor, const uint32_t *toDo,
                                const uint32_t toDoStart_old, const uint32_t toDoStart_new) {
    uint32_t threadId = threadIdx.x + blockDim.x * blockIdx.x;
    uint32_t toDoOffset = threadId + toDoStart_old;

    if (toDoOffset < toDoStart_new) {
        uint32_t nodeId = toDo[toDoOffset];
        C[nodeId] = currentColor;
    }
}

void ColorerChKernels::lubyIndexesWithStructure(const GraphStruct<col, col> *const str, uint32_t *const C, uint32_t threadsPerBlock_x) {
    uint32_t nNodes = str->nNodes;

    uint32_t *toColor_d;
    cudaError cuSts = cudaMalloc(&toColor_d, sizeof(uint32_t) * nNodes);
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t *toDo_d;
    cuSts = cudaMalloc(&toDo_d, sizeof(uint32_t) * nNodes);
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t *shuffle_d;
    cuSts = cudaMalloc(&shuffle_d, sizeof(uint32_t) * nNodes);
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t *toDoPermutated_d;
    cuSts = cudaMalloc(&toDoPermutated_d, sizeof(uint32_t) * nNodes);
    cudaCheck(cuSts, __FILE__, __LINE__);

    uint32_t currentColor = 0;

    uint32_t toDoStart = 0;
    uint32_t toDoStart_old = 0;

    dim3 threadsPerBlock = dim3(threadsPerBlock_x, 1, 1);
    dim3 blocksPerGrid = dim3((nNodes + threadsPerBlock.x - 1) / threadsPerBlock.x, 1, 1);

    lubyIndexesInitToDo <<< blocksPerGrid, threadsPerBlock >>> (str, toDo_d);

    do {
        blocksPerGrid = dim3((nNodes + threadsPerBlock.x - 1 - toDoStart) / threadsPerBlock.x, 1, 1);
        currentColor++;

        lubyIndexesIsToColor <<< blocksPerGrid, threadsPerBlock >>> (str, C, toColor_d, toDoStart, toDo_d);

        thrust::inclusive_scan(thrust::device, toColor_d + toDoStart, toColor_d + nNodes, shuffle_d + toDoStart);

        uint32_t max;
        cuSts = cudaMemcpy(&max, &shuffle_d[nNodes - 1], sizeof(uint32_t), cudaMemcpyDeviceToHost);
        cudaCheck(cuSts, __FILE__, __LINE__);

        lubyIndexesToDoIndirectPermutation <<< blocksPerGrid, threadsPerBlock >>> (str, toDoStart, shuffle_d, toColor_d, max);

        lubyIndexesToDoPermutation <<< blocksPerGrid, threadsPerBlock >>> (str, toDoStart, shuffle_d, toDo_d, toDoPermutated_d);

        cuSts = cudaMemcpy(toDo_d, toDoPermutated_d, sizeof(uint32_t) * nNodes, cudaMemcpyDeviceToDevice);
        cudaCheck(cuSts, __FILE__, __LINE__);

        toDoStart_old = toDoStart;
        toDoStart += max;

        blocksPerGrid = dim3((toDoStart - toDoStart_old + threadsPerBlock.x - 1) / threadsPerBlock.x, 1, 1);
        lubyIndexesColor <<< blocksPerGrid, threadsPerBlock >>> (C, currentColor, toDo_d, toDoStart_old, toDoStart);
    } while(toDoStart < nNodes);

    cudaFree(toColor_d);
    cudaFree(toDo_d);
    cudaFree(shuffle_d);
    cudaFree(toDoPermutated_d);
}
