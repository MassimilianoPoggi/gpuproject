#include <iostream>
#include <iomanip>
#include "ColorerCh.h"

ColorerCh::ColorerCh( Graph<col,col>* g, GPURand * GPURandomizer ) : Colorer(g), str { graph->getStruct() },
 		n { str->nNodes }, GPUrndmzer { GPURandomizer }, nCol { 0 } {

	// Host stuff
	coloring 	= new Coloring();
	C_h			= new uint32_t[n];

	// Cuda stuff
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	numThreads		= 32;
	threadsPerBlock	= dim3(numThreads, 1, 1);
	blocksPerGrid	= dim3((n + numThreads - 1) / numThreads, 1, 1);

	cuSts = cudaMalloc( &C_d,			n * sizeof(uint32_t)); cudaCheck(cuSts, __FILE__, __LINE__);
	cuSts = cudaMemset( C_d,		 0,	n * sizeof(uint32_t)); cudaCheck(cuSts, __FILE__, __LINE__);

	// Temporary stuff
    GPUMemTracker::coloringColorsSize		= n * sizeof(uint32_t);

}

ColorerCh::~ColorerCh() {
	cudaFree( C_d );

	delete[] C_h;
	delete coloring;
}

void ColorerCh::run(uint32_t threadsPerBlock, uint32_t algonum) {
	std::cout << "\n\033[32;1m**    RUN!   **\033[0m" << std::endl;
	cudaEventRecord(start);

	switch (algonum){
		case 0:
			ColorerChKernels::lubyIndexesSimple(str, C_d, threadsPerBlock);
			break;
		case 1:
			ColorerChKernels::lubyIndexesWithStructure(str, C_d, threadsPerBlock);
			break;
		case 2:
			ColorerChKernels::lubyRandomSimple(str, C_d, GPUrndmzer->randStates, threadsPerBlock);
			break;
		case 3:
			ColorerChKernels::lubyRandomWithStructure(str, C_d, GPUrndmzer->randStates, threadsPerBlock);
			break;
		case 4:
			ColorerChKernels::jonesPlassmanSimple(str, C_d, GPUrndmzer->randStates, threadsPerBlock);
			break;
		case 5:
			ColorerChKernels::jonesPlassmanWithStructure(str, C_d, GPUrndmzer->randStates, threadsPerBlock);
			break;
		case 6:
			ColorerChKernels::largestDegreeSimple(str, C_d, threadsPerBlock);
			break;
		case 7:
			ColorerChKernels::largestDegreeWithStructure(str, C_d, threadsPerBlock);
			break;

	}

	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, start, stop);
	std::cout << "GPU time: " << milliseconds/1000.0f << std::endl;

}

void ColorerCh::testCorrectColoring(bool verbose) {

	cuSts = cudaMemcpy( C_h, C_d, n * sizeof(uint32_t), cudaMemcpyDeviceToHost );
	const uint32_t * const C = C_h;		// Per la lambda!

	// stampa la colorazione
	if (verbose) {
		std::cout << "Colorazione: ";
		std::for_each(C_h, C_h + n, [](uint32_t cc) { std::cout << cc << " "; });
		std::cout << std::endl;
	}
    int maxColor = 0;

	for( uint32_t i = 0; i < n; i++ ) {
		uint32_t currentColor = C[i];
		if (currentColor == 0){
		    std::cout << "\033[31;1mNodo " << i << "\033[0m" << std::endl;
		} else if (currentColor > maxColor){
		    maxColor = currentColor;
		}
		uint64_t nodeDeg = str->cumulDegs[i + 1] - str->cumulDegs[i];
		const uint32_t * const firstNeigh = &(str->neighs[str->cumulDegs[i]]);
		std::for_each(firstNeigh, firstNeigh + nodeDeg, [currentColor, i, C](uint32_t nodeNeigh) {if ((C[nodeNeigh] == currentColor) & (nodeNeigh > i))
					std::cout << "\033[31;1mConflitto tra il nodo " << i << " e " << nodeNeigh << " - colore: " << currentColor << "\033[0m" << std::endl;} );
	}
    std::cout << "Max color: " << maxColor << std::endl;
}
