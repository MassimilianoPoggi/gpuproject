#pragma once
#include <memory>
#include "graph/graph.h"


typedef uint32_t col;     // node color
typedef uint32_t col_sz;     // node color

#define FOR_ALL_NODE for (node i = 0; i < n; i++)   // loop on graph nodes
#define FOR_ALL_COL_0 for (col c = 0; c < nCol; c++)  // loop on coloring colors

// graph coloring
struct Coloring {
	uint32_t	 		nCol;		// num of color classes
	uint32_t		*	colClass;	// list (array) of all node colors class by class
	uint32_t		*	cumulSize;	// cumulative color class sizes

	/// return the size of class c
	uint32_t classSize(col c) {
		return cumulSize[c] - cumulSize[c-1];
	}
};

/**
 * Represents a coloring for a graph
 */
class Colorer {
public:
	Colorer(Graph<uint32_t,uint32_t>* g) : graph{g} {
		std::default_random_engine eng {};
		std::uniform_real_distribution<> randU(0.0, 1.0);
		seed = static_cast<float>( randU(eng) );
	}
	virtual ~Colorer() {};
	virtual void run(uint32_t threadsPerBlock, uint32_t algonum) = 0;
	float efficiencyNumProcessors(unsigned);
	Coloring* getColoring();
	bool checkColoring();
	col_sz getNumColor() const;
	void buildColoring(col*, node_sz);
	void print(bool);
	double getElapsedTime() {return elapsedTimeSec;};
	void setSeed(float s) {seed = s;};
	void verbose() {verb = 1;};

protected:
	std::string name;
	Graph<col,col>* graph;
	Coloring* coloring{};
	float* meanClassDeg{};	  // mean degs of class nodes
	float meanClassDegAll{0}; // mean deg over all class nodes
	float stdClassDegAll{0};  // std of deg of all IS nodes
	double elapsedTimeSec{0};
	float seed{0};            // for random generator
	bool verb{0};
};

/**
 *  Get a greedy colorings of a graph
 */
class ColoringGeedyCPU: public Colorer {
public:
	ColoringGeedyCPU(Graph<col,col>* g) : Colorer(g) {
		coloring = new Coloring();
		name = "Greedy-CPU";
	}
	void run(uint32_t threadsPerBlock, uint32_t algonum) override;
	~ColoringGeedyCPU() {delete coloring; delete[] meanClassDeg;}
};
