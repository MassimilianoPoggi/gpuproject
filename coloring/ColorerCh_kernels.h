#pragma once
#include <memory>
#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>
#include "coloring.h"

namespace ColorerChKernels {
	void lubyIndexesSimple(const GraphStruct<col, col> * const str, uint32_t * const C, uint32_t threadsPerBlock);
	void lubyIndexesWithStructure(const GraphStruct<col, col> * const str, uint32_t * const C, uint32_t threadsPerBlock);

	void lubyRandomSimple(const GraphStruct<col, col> * const str, uint32_t * const C, curandState * GPUrnd, uint32_t threadsPerBlock);
	void lubyRandomWithStructure(const GraphStruct<col, col> * const str, uint32_t * const C, curandState * GPUrnd, uint32_t threadsPerBlock);

	void jonesPlassmanSimple(const GraphStruct<col, col> * const str, uint32_t * const C, curandState * GPUrnd, uint32_t threadsPerBlock);
	void jonesPlassmanWithStructure(const GraphStruct<col, col> * const str, uint32_t * const C, curandState * GPUrnd, uint32_t threadsPerBlock);

	void largestDegreeSimple(const GraphStruct<col, col> * const str, uint32_t * const C, uint32_t threadsPerBlock);
	void largestDegreeWithStructure(const GraphStruct<col, col> * const str, uint32_t * const C, uint32_t threadsPerBlock);
}
