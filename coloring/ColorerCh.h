#pragma once
#include <memory>
#include <algorithm>
#include <cuda.h>
#include <cuda_runtime.h>
#include <curand_kernel.h>
#include "coloring.h"
#include "ColorerCh_kernels.h"
#include "graph/graph.h"
#include "GPUutils/GPUutils.h"

class ColorerCh: public Colorer {

public:
	ColorerCh( Graph<col,col>* g, GPURand * GPURandomizer );
	~ColorerCh();

	void run(uint32_t threadsPerBlock, uint32_t algonum) override;
	void testCorrectColoring(bool verbose);

private:
	const GraphStruct<col, col>	*	const	str;	// graph structure
	const uint32_t							n;		// numero nodi del grafo

	uint32_t					*			C_h;	// output: array della colorazione; size = n; allocato su host
	uint32_t								nCol;	// output: numero di colori trovato

	// Cuda stuff
	cudaError								cuSts;
	cudaEvent_t 							start, stop;
	uint32_t								numThreads;
	dim3									threadsPerBlock, blocksPerGrid;

	GPURand						*			GPUrndmzer;
	uint32_t					*			C_d;	// size = n; per ogni nodo, indica il suo colore; allocato in device

};
