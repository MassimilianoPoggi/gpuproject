#include "GPUutils.h"
#include "GPURandomizer.h"


__global__ void GPURand_k::initCurand(curandState* states, unsigned int seed, unsigned int nElem ) {
	unsigned int tid = threadIdx.x + blockDim.x * blockIdx.x;
	if (tid < nElem) {
		curand_init( seed, tid, 0, &states[tid] );
	}
}


GPURand::GPURand( int n, long seed ) : num( n ), seed( seed ) {

    // configuro la griglia e i blocchi
	dim3 threadsPerBlock( 32, 1, 1 );
	dim3 blocksPerGrid( (n + threadsPerBlock.x - 1) / threadsPerBlock.x, 1, 1 );

    // alloco abbastanza generatori di numeri casuali e li inizializzo
	cuSts = cudaMalloc( (void **)&randStates, n * sizeof( curandState ) ); cudaCheck( cuSts, __FILE__, __LINE__ );
	//GPURand_k::initCurand <<< blocksPerGrid, threadsPerBlock >>> (randStates, 11, n);
    //GPURand_k::initCurand <<< blocksPerGrid, threadsPerBlock >>> (randStates, time(NULL), n);
	GPURand_k::initCurand << < blocksPerGrid, threadsPerBlock >> > (randStates, seed, n);
	cudaDeviceSynchronize();

	// init generatore per chiamate host
	/*curandSts = curandCreateGenerator( &gen, CURAND_RNG_PSEUDO_DEFAULT ); curandCheck( curandSts, __FILE__, __LINE__ );
	curandSts = curandSetPseudoRandomGeneratorSeed( gen, seed ); curandCheck( curandSts, __FILE__, __LINE__ );*/
}

GPURand::~GPURand() {
	cuSts = cudaFree( randStates ); cudaCheck( cuSts, __FILE__, __LINE__ );
	//curandSts = curandDestroyGenerator( gen ); curandCheck( curandSts, __FILE__, __LINE__ );
}
