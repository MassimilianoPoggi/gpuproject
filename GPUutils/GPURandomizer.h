#pragma once
#ifdef WIN32
#include <cuda.h>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#endif

#include <random>
#include <curand.h>
#include <curand_kernel.h>

namespace GPURand_k {
    __global__ void initCurand(curandState*, unsigned int, unsigned int);
}


class GPURand {
public:

	GPURand( int n, long seed );
	~GPURand();

	int						num;
	unsigned int			seed;
	cudaError_t             cuSts;
	curandStatus_t          curandSts;
	curandState         *   randStates;
	curandGenerator_t       gen;

};
